from pathlib import Path

# Setup file paths
this_dir = Path.cwd()
root_dir = this_dir.parent
simulation_dir = root_dir / "experiments" / "riid_templates"
mimbs_dir = root_dir / "experiments" / "mimbs" 

# Add to sys path
import sys
sys.path.append(str(root_dir))
sys.path.append(str(simulation_dir))

# Bokeh
from bokeh import plotting as plt
plt.output_notebook()
from bokeh.palettes import d3
from analysis.plotting import Plotting

# Some global plotting parameters
PLOT_HEIGHT = 450
plot_params = {'plot_width': 1400, 'plot_height': PLOT_HEIGHT}
plot_palette = d3['Category10']
plt = Plotting(params=plot_params, palette=plot_palette)

# Templates
from load_templates import load_templates, TemplateInfo
from analysis.spectrum import smooth_binomial, make_histogram
from eti9305 import nph_to_adc

n_bins = 1024
template_to_run = load_templates(simulation_dir/"runs")

import numpy as np
import pandas as pd

# Calibration
from analysis.fitting import quadratic, apply
template_calibration_beta = np.load(simulation_dir/'template_calibration.npy')
calibration_template = apply(quadratic, template_calibration_beta)
channels = np.arange(n_bins)
energies = calibration_template(channels*2)

# Fitting
import pickle

from collections import namedtuple
from re import compile
from analysis.fitting import gauss

with open(mimbs_dir / "fits.pickle", "rb") as f:
    path_to_fit = pickle.load(f)
with open(mimbs_dir / "samples.pickle", "rb") as f:
    path_to_sample = pickle.load(f)
with open(mimbs_dir / "sources.pickle", "rb") as f:
    sources = pickle.load(f)
with open(mimbs_dir / "absorbers.pickle", "rb") as f:
    absorbers = pickle.load(f)
    
    
def relative_activities(a):
    """Number of detections in template * concentration of isotope, for all isotopes, normalised by total"""
    activities = np.array([template_to_run[(s, None)].detections*ai for s, ai in zip(sources, a)])
    return activities / activities.sum()


def repr_fit(fit, tol_a: float=1e-3, tol_t: float=1e-3) -> str:
    a = fit.isotope_density
    t = fit.basis_density
    source_pairs = sorted(zip(relative_activities(a), sources), reverse=True)
    source_fields = [f"{a_i*100:.2f}% {s}" for a_i, s in source_pairs if a_i/a.sum() > tol_a]
    shield_pairs = sorted(zip(t, absorbers), reverse=True)
    shield_fields = [f"{t_i:.2f} {s}" for t_i, s in shield_pairs if t_i > tol_t]
    return f"{', '.join(source_fields)} // {', '.join(shield_fields)}"

channels_fit = np.arange(2048)
energies_fit = calibration_template(channels_fit)