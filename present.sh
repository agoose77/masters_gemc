#!/usr/bin/env bash

trap ctrl_c INT

function ctrl_c() {
        kill $p_mouse
	kill $p_jup
	kill $p_chrome
	kill $p_dispy
}


java -jar presentation/mouse_server.jar &
p_mouse=$!
jupyter notebook --no-browser &
p_jup=$!
optirun -b primus google-chrome --ignore-gpu-blacklist --disable-gpu-sandbox --kiosk http://localhost:8888/notebooks/presentation/Presentation.ipynb &
p_chrome=$!
./serve.sh
p_dispy=$!

