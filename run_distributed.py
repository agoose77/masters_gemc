#!/usr/bin/env python3.6
"""Run Geant4 simulation across local network using dispy"""

from argparse import ArgumentParser
from collections import namedtuple
from datetime import datetime
from json import dumps, loads
from os import chdir, makedirs
from pathlib import Path
from random import getrandbits
from typing import Dict, Any, Tuple, List, Union

from netifaces import ifaddresses, interfaces, AF_INET

from dispy import JobCluster
from dispy.httpd import DispyHTTPServer

from simulation import METRIC_TYPES

Config = namedtuple("Config", "specification_path dependency_paths")

WAIT_CHECK_FINISH_TIMEOUT = 1 / 60
CLOSE_WAIT_TIMEOUT = 3.0

RUN_INFO_FILE_NAME = 'run_info.json'
JOB_ID_TEMPLATE = "batch_{batch}_do_{limit}"
JOB_OUTPUT_FILE_NAME_TEMPLATE = "{job.id}.json"
JOB_DEST_PATH_NAME = "DISPY_JOB"
MACROS_DIR = Path() / 'macros'

VERSION = (0, 0, 1)


def get_seed() -> int:
    return getrandbits(32)


def job_computation(specification_path: str, seed: int, metric_type: str, metric_limit: int,
                    macro_paths: 'List[Path]' = None,
                    **specification_kwargs: 'Dict[str, Any]'):
    from pathlib import Path
    from simulation import run_simulation, load_specification_namespace, initialise_specification_from_namespace

    file_path = Path(specification_path)
    specification = initialise_specification_from_namespace(load_specification_namespace(file_path), seed,
                                                            **specification_kwargs)
    return run_simulation(specification, metric_type, metric_limit, macro_paths)


def submit_jobs_to_cluster(cluster: JobCluster, specification_path: Path, metric_type: str, metric_limit: int,
                           job_metric_limit: int, macro_paths: List[Path], **specification_kwargs: Dict[str, Any]) -> \
List['SubmittedJob']:
    """Submit the appropriate number of jobs to the Dispy cluster, according to the total simulation metric,
    and per-job metric limit.

    For example, to simulate 10_000_000 events, with a job limit of 1000 would lead to 10_000 jobs.
    """
    # When handling files, any which are outside CWD have their leading elements removed

    # Convert from Path objects to str, and resolve them as dispy will when sending via file transfer
    root_directory = Path.cwd()
    resolved_macro_path_strings = [resolve_dependency_path_as_dispy(root_directory, p) for p in macro_paths]
    resolved_specification_path_string = resolve_dependency_path_as_dispy(root_directory, specification_path)

    remaining_limit = metric_limit
    submitted_jobs = []

    job_kwargs = {'specification_path': resolved_specification_path_string, 'metric_type': metric_type,
                  'macro_paths': resolved_macro_path_strings, **specification_kwargs}
    while remaining_limit:
        seed = get_seed()

        # Each job consumes from global limit
        actual_job_limit = min(job_metric_limit, remaining_limit)
        remaining_limit -= actual_job_limit

        # Kwargs unique to this job
        job_unique_kwargs = {'seed': seed, 'metric_limit': actual_job_limit}

        job = cluster.submit(**job_kwargs, **job_unique_kwargs)
        job.id = JOB_ID_TEMPLATE.format(batch=len(submitted_jobs), limit=actual_job_limit)

        submitted_jobs.append(SubmittedJob(job, job_unique_kwargs))

    return submitted_jobs


class WaitResultTimeout(Exception):
    pass


class SubmittedJob:
    def __init__(self, dispy_job, job_kwargs):
        self.dispy_job = dispy_job
        self.kwargs = job_kwargs

    def wait_for_result(self, timeout: float, raise_set_exception: bool = True):
        if not self.dispy_job.finish.wait(timeout):
            raise WaitResultTimeout

        if self.dispy_job.exception and raise_set_exception:
            raise Exception(self.dispy_job.exception) from None


def write_finished_job(directory: Path, submitted_job: SubmittedJob):
    """Write finished job to directory, using the job ID and JOB_FILE_EXT for filename"""
    dispy_job = submitted_job.dispy_job
    job_kwargs = submitted_job.kwargs

    info = {
        'job_id': dispy_job.id,
        'job_args': job_kwargs,
        'result': dispy_job.result
    }
    info_json = dumps(info, sort_keys=True, indent=4)
    file_name = JOB_OUTPUT_FILE_NAME_TEMPLATE.format(job=dispy_job)
    (directory / file_name).write_text(info_json)


def resolve_dependency_path_as_dispy(root_directory: Path, path: Path) -> str:
    """Resolve dependency path as dispy will internally"""
    try:
        return str(path.resolve().relative_to(root_directory))
    except ValueError:
        return path.name


def write_jobs_on_finish(output_directory: Path, submitted_jobs: List[SubmittedJob],
                         check_finish_timeout: float = WAIT_CHECK_FINISH_TIMEOUT):
    """Write each job to disk as it finishes, blocking until all jobs are finished.

    Will raise exception detailing error in completing any job
    """
    pending_submitted_jobs = set(submitted_jobs)

    while pending_submitted_jobs:
        submitted_job = pending_submitted_jobs.pop()

        try:
            submitted_job.wait_for_result(check_finish_timeout)
        except WaitResultTimeout:
            pending_submitted_jobs.add(submitted_job)
            continue

        write_finished_job(output_directory, submitted_job)


def create_jobs_directory(output_directory: Path, message: str = "") -> Path:
    """Create the directory in which jobs will be written, according to the current timestamp and given message"""
    this_simulation_directory = output_directory / f"{datetime.now():%d-%m-%y %H:%M:%S}#{message}"
    makedirs(this_simulation_directory)
    return this_simulation_directory


def get_local_ip() -> str:
    for iface in interfaces():
        for addr_info in ifaddresses(iface).get(AF_INET, []):
            ipv4_addr = addr_info['addr']
            if ipv4_addr != '127.0.0.1':
                return ipv4_addr


def get_metric_from_args(args) -> Tuple[str, int]:
    """Find the metric type and its corresponding value from the args namespace"""
    metric_args = [(k, getattr(args, k)) for k in METRIC_TYPES if getattr(args, k) is not None]
    assert len(metric_args) == 1, "Require one, and only one, terminating metric value"
    return metric_args.pop()


def resolve_paths(path_strings: List[Union[str, Path]]) -> List[Path]:
    """Resolve list of string / Path objects which may contain glob-style wildcards"""
    paths = []
    for path_str in path_strings:
        path = Path(path_str).resolve()
        paths.extend((p.resolve()) for p in path.parent.glob(path.name))
    return paths


def load_config(config_path: Path) -> Config:
    """Load configuration object from JSON file.

    Return Config object with absolute paths to any referenced macros, dependencies or specifications.
    Validate any dependencies to ensure they share the same parent as the specification file
    """
    data = loads(config_path.read_text())
    specification_directory = config_path.parent

    absolute_specification_path = (specification_directory / data['specification']).resolve()
    dependency_path_patterns = [specification_directory / p for p in data['dependencies']]
    absolute_dependency_paths = resolve_paths(dependency_path_patterns)

    return Config(absolute_specification_path, absolute_dependency_paths)


def write_run_info_to_disk(output_directory: Path, config: Config, metric_type: str, metric_limit: int,
                           job_metric_limit: int, macro_paths: List[Path], specification_kwargs: Dict[str, Any]):
    data = {'specification_kwargs': specification_kwargs,
            'macro_paths': [str(p) for p in macro_paths],
            'metric_type': metric_type,
            'metric_limit': metric_limit,
            'job_metric_limit': job_metric_limit,
            'specification_path': str(config.specification_path),
            'dependency_paths': [str(p) for p in config.dependency_paths],
            'version': VERSION}

    run_info_path = (output_directory / RUN_INFO_FILE_NAME)
    run_info_path.write_text(dumps(data))


def run_distributed(config: Config, metric_type: str, metric_limit: int, job_metric_limit: int,
                    jobs_output_directory: Path, macro_paths: List[Path] = None,
                    **specification_kwargs: Dict[str, Any]):
    # Ensure we have a macro paths list, and resolve them (to make absolute and deal with wild cards)
    if macro_paths is None:
        macro_paths = []
    resolved_macro_paths = resolve_paths(macro_paths)
    jobs_output_directory = jobs_output_directory.resolve()

    write_run_info_to_disk(jobs_output_directory, config, metric_type, metric_limit, job_metric_limit,
                           macro_paths, specification_kwargs)

    # Ugly fix to ensure that dispy resolves file paths relative to the specification file, not the this module
    chdir(config.specification_path.parent)

    # Find all the dependencies
    dependency_paths = [config.specification_path, *resolved_macro_paths, *config.dependency_paths]

    # Create cluster and management server
    cluster = JobCluster(job_computation,
                         depends=[str(p) for p in dependency_paths],
                         ip_addr=get_local_ip())
    http_server = DispyHTTPServer(cluster, poll_sec=3)

    try:
        # Create all jobs & write them to disk as they complete
        submitted_jobs = submit_jobs_to_cluster(cluster, config.specification_path, metric_type, metric_limit,
                                                job_metric_limit, resolved_macro_paths, **specification_kwargs)
        write_jobs_on_finish(jobs_output_directory, submitted_jobs)

    except:
        http_server.shutdown(wait=False)
        cluster.close(timeout=CLOSE_WAIT_TIMEOUT, terminate=True)
        raise

    else:
        http_server.shutdown()
        cluster.close()


if __name__ == "__main__":
    from simulation import extract_kwargs_from_commandline, load_specification_namespace

    parser = ArgumentParser()
    parser.add_argument("config", type=Path, help="config file for simulation")
    parser.add_argument("-d", type=Path, help="output directory for jobs folder", dest="dir")

    for metric in METRIC_TYPES:
        parser.add_argument(f"-{metric}", help=f'define termination criterion metric value', type=int)

    parser.add_argument("-j", help="job limit", type=int, default=100, dest='job_limit')
    parser.add_argument("-i", help="message to include in output dir name", type=str, default='', dest='message')
    parser.add_argument("-x", help="include macro", action='append', type=Path, dest='macros')
    args, unknown_args = parser.parse_known_args()

    metric, metric_limit = get_metric_from_args(args)
    # Load job configuration
    assert args.config.exists()
    config = load_config(args.config)

    namespace = load_specification_namespace(config.specification_path)
    specification_kwargs = extract_kwargs_from_commandline(namespace.configure_simulation, unknown_args)

    if args.dir is None:
        output_directory = create_jobs_directory(Path.cwd(), args.message)
    else:
        output_directory = create_jobs_directory(args.dir, args.message)

    run_distributed(config, metric, metric_limit, args.job_limit, output_directory, args.macros, **specification_kwargs)
