#!/usr/bin/env bash
# Serve dispy on LAN
python3 -m dispy.dispynode -i $(ip route get 1 | awk '{print $NF;exit}') --clean