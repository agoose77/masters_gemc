from pathlib import Path
from typing import Any, Dict, Union

import numpy as np
from Geant4 import (G4ThreeVector, mm, eV, rad, G4MaterialPropertiesTable, G4OpticalSurface,
                    G4OpticalSurfaceFinish, G4OpticalSurfaceModel, G4SurfaceType, G4LogicalBorderSurface,
                    G4LogicalSkinSurface, MeV, cm, G4Material, cm3, ns, g, G4RotationMatrix, deg, G4VisAttributes,
                    G4Color, G4SDManager, G4EmPenelopePhysics, G4OpticalPhysics,
                    G4HadronPhysicsFTFP_BERT, G4DecayPhysics, G4RadioactiveDecayPhysics)
from detector import getHCFromVirtual, countOfType, getEDepArray, getPDGArray, SensitiveDetector

PARENT_DIRECTORY = Path(__file__).parent

BEAM_ENERGY: float = None


def configure_simulation(energy: float, ):
    global BEAM_ENERGY

    BEAM_ENERGY = energy


def get_detection_result(event) -> Union[Dict[str, Any], None]:
    hc_of_this_event = event.GetHCofThisEvent()
    assert hc_of_this_event

    vhc_photocathode = get_hit_collection(hc_of_this_event, "Photocathode")
    vhc_crystal = get_hit_collection(hc_of_this_event, "Crystal")

    hc_crystal = getHCFromVirtual(vhc_crystal)
    not_photons = getPDGArray(hc_crystal) != 0
    e_dep = getEDepArray(hc_crystal)[not_photons].sum()

    hc_photocathode = getHCFromVirtual(vhc_photocathode)
    photocathode_hits = countOfType(hc_photocathode, "opticalphoton")

    if not photocathode_hits:
        return None

    return {'photocathode': photocathode_hits, 'crystal': e_dep}


def define_detectors(detector_builder):
    detector_world = detector_builder.create_box("World", 30 * cm, 80 * cm, 30 * cm, "G4_AIR", logical_mother=None,
                                                 attributes=G4VisAttributes.GetInvisible())

    dp_detector = G4ThreeVector(0.0, 26.5, 5.1 - 8.1915 / 2.0) * cm
    build_ortec_905_4(detector_builder, detector_world, dp_detector)


def define_physics_constructors(physics_list):
    optical = G4OpticalPhysics()
    optical.SetWLSTimeProfile("delta")
    yield optical
    yield G4HadronPhysicsFTFP_BERT()
    yield G4EmPenelopePhysics()
    yield G4DecayPhysics()
    yield G4RadioactiveDecayPhysics()
    physics_list.SetVerboseLevel(0)


def define_materials(nist_manager):
    air = nist_manager.FindOrBuildMaterial("G4_AIR")

    photon_energies_single = [1.12713 * eV]
    mpt_air = G4MaterialPropertiesTable()
    mpt_air.AddProperty("RINDEX", photon_energies_single, [1.000293])
    mpt_air.AddProperty("ABSLENGTH", photon_energies_single, [10 * cm])
    air.SetMaterialPropertiesTable(mpt_air)

    # Bialkali PMT material
    bialkali = compose_material(nist_manager, "Bialkali", 4.28 * g / cm3,
                                {'G4_K': 0.133, 'G4_Cs': 0.452, 'G4_Sb': 0.415})
    bialkali_properties = G4MaterialPropertiesTable()
    bialkali_properties.AddProperty("RINDEX", photon_energies_single, [1.5])
    bialkali_properties.AddProperty("ABSLENGTH", photon_energies_single, [1e-4 * mm])
    bialkali.SetMaterialPropertiesTable(bialkali_properties)



    # Scintillator material
    nai_tl_density_g_cm3 = 3.67
    sodium_iodide_tl = compose_material(nist_manager, "NaI(Tl)", nai_tl_density_g_cm3 * g / cm3,
                                        {'G4_SODIUM_IODIDE': 0.996, 'G4_Tl': 0.004})  # [1]

    # Define scintillator material properties table
    scintillator_properties = G4MaterialPropertiesTable()

    # [0] Load intensity profile
    INTENSITIES_PATH = PARENT_DIRECTORY / "data/naitl_intensity.csv"
    intensity_wavelengths_nm, intensities = np.loadtxt(INTENSITIES_PATH, delimiter=',').T
    energies_intensity = (nm_to_ev(intensity_wavelengths_nm) * eV).tolist()
    intensities = intensities.tolist()

    scintillator_properties.AddProperty("FASTCOMPONENT", energies_intensity, intensities)
    scintillator_properties.AddProperty("ABSLENGTH", [energies_intensity[0]], [1000 * cm])  # [2]
    scintillator_properties.AddProperty("RINDEX", [energies_intensity[0]], [1.85])  # [1]
    scintillator_properties.AddConstProperty("SCINTILLATIONYIELD", 10000 / MeV)  # [1] 38/kev
    scintillator_properties.AddConstProperty("FASTTIMECONSTANT", 28.23 * ns)  # [5]
    scintillator_properties.AddConstProperty("YIELDRATIO", 1.0)
    scintillator_properties.AddConstProperty("RESOLUTIONSCALE", 1.0)
    ion = sodium_iodide_tl.GetIonisation()
    ion.SetBirksConstant(3.8e-3 * mm / MeV)  # [6]
    sodium_iodide_tl.SetMaterialPropertiesTable(scintillator_properties)


def configure_particle_gun(particle_gun):
    particle_gun.SetParticleByName("gamma")
    particle_gun.SetParticleEnergy(
        BEAM_ENERGY * MeV)  # VERY important, seems default has energy which leads to wrong processes
    particle_gun.SetParticlePosition(G4ThreeVector(0.0, 0.0, 0.0) * mm)
    particle_gun.SetParticleMomentumDirection(G4ThreeVector(0., 10., 0.))


# User routines
def get_hit_collection(hc_of_this_event, sensitive_detector_name: str):
    sd_manager = G4SDManager.GetSDMpointer()
    hc_table = sd_manager.GetHCtable()
    hc_path = f"{sensitive_detector_name}/{sensitive_detector_name}"
    collection_id = hc_table.GetCollectionID(hc_path)
    return hc_of_this_event.GetHC(collection_id)


def nm_to_ev(wavelength):
    """E=hf, f=c/λ, E=hc/λ, V=hc/(λq)"""
    c = 299_792_458
    _q = 1.60217662
    _h = 6.62607004
    return (_h * c * 1e-6) / (wavelength * _q)


def build_ortec_905_4(detector_builder, detector_world, displacement: G4ThreeVector):
    length_crystal = 76.2 * mm
    radius_crystal = 76.2 * mm / 2.0
    length_pmt = 2 * mm
    thickness_wall = 0.508 * mm
    radius_wall = 81.915 * mm / 2.0  # (81.915)/2-.508

    rotation = G4RotationMatrix()
    rotation.rotateX(90 * deg)

    # Translation rel to source
    translation = G4ThreeVector(0.0, (length_crystal + length_pmt + thickness_wall) / 2, 0.0) + displacement

    detector_detector = detector_builder.create_cylinder("Detector", 0.0, radius_wall,
                                                         length_crystal + length_pmt + thickness_wall, "G4_AIR",
                                                         detector_world.logical,
                                                         rotation=rotation,
                                                         translation=translation,
                                                         attributes=G4VisAttributes(G4Color(0, 0, 1, .4)))

    detector_builder.create_cylinder("Wall", radius_wall - thickness_wall, radius_wall,
                                     length_crystal + length_pmt, 'G4_Al', detector_detector.logical,
                                     translation=G4ThreeVector(0, 0, +thickness_wall / 2),
                                     attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_wall_cap = detector_builder.create_cylinder("WallCap", 0.0, radius_wall, thickness_wall, 'G4_Al',
                                                         detector_detector.logical,
                                                         translation=G4ThreeVector(0, 0.0,
                                                                                   -(length_pmt + length_crystal) / 2),
                                                         attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_crystal = detector_builder.create_cylinder("Crystal", 0.0, radius_crystal, length_crystal, 'NaI(Tl)',
                                                        detector_detector.logical,
                                                        translation=G4ThreeVector(0.0, 0.0,
                                                                                  -(length_pmt - thickness_wall) / 2))
    detector_photocathode = detector_builder.create_cylinder("Photocathode", 0.0, radius_crystal, length_pmt,
                                                             "Bialkali",
                                                             detector_detector.logical,
                                                             translation=G4ThreeVector(0.0, 0.0, (
                                                                 thickness_wall + length_crystal - 1e-3) / 2),
                                                             attributes=G4VisAttributes(G4Color(1, 0, 0)))

    detector_photocathode.make_sensitive(SensitiveDetector("Photocathode"))
    detector_crystal.make_sensitive(SensitiveDetector("Crystal"))

    # Wrapping
    sigma_alpha = 0.23 * rad  # https://www.osapublishing.org/oe/abstract.cfm?uri=oe-19-5-4199
    wrap_surface = G4OpticalSurface("Tyvek", G4OpticalSurfaceModel.unified, G4OpticalSurfaceFinish.groundtyvekair,
                                    G4SurfaceType.dielectric_metal, sigma_alpha)

    mpt_surface = G4MaterialPropertiesTable()
    surface_photon_energies = [1.12713 * eV]
    mpt_surface.AddProperty("REFLECTIVITY", surface_photon_energies, [0.975])
    mpt_surface.AddProperty("EFFICIENCY", surface_photon_energies, [0.0])
    wrap_surface.SetMaterialPropertiesTable(mpt_surface)
    G4LogicalBorderSurface("Tyvek", detector_crystal.physical, detector_detector.physical, wrap_surface)
    G4LogicalBorderSurface("Tyvek Cap", detector_crystal.physical, detector_wall_cap.physical, wrap_surface)

    # Photocathode optical properties
    photocathode_surface = G4OpticalSurface("PhotocathodeSurface", G4OpticalSurfaceModel.glisur,
                                            G4OpticalSurfaceFinish.polished, G4SurfaceType.dielectric_metal)
    eneriges_photocathode = [5.0 * eV]

    # load efficiencies
    EFFICIENCIES_PATH = PARENT_DIRECTORY / "data/qe_pmt_eti9305.csv"
    efficiency_wavelengths_nm, efficiencies = np.loadtxt(EFFICIENCIES_PATH, delimiter=',').T
    efficiency_energies = (nm_to_ev(efficiency_wavelengths_nm) * eV).tolist()
    efficiencies = efficiencies.tolist()

    bialkali_properties = G4Material.GetMaterial('Bialkali').GetMaterialPropertiesTable()
    photocathode_properties = copy_material_properties(bialkali_properties)
    photocathode_properties.AddProperty("EFFICIENCY", efficiency_energies, efficiencies)
    photocathode_properties.AddProperty("REFLECTIVITY", eneriges_photocathode, [0.0])
    photocathode_surface.SetMaterialPropertiesTable(photocathode_properties)
    G4LogicalSkinSurface("PhotocathodeSkin", detector_photocathode.logical, photocathode_surface)


def copy_material_properties(properties):
    new_properties = G4MaterialPropertiesTable()
    for name, vec in properties.GetPropertiesMap().items():
        new_properties.AddProperty(str(name), vec)
    for name, value in properties.GetPropertiesCMap().items():
        new_properties.AddConstProperty(str(name), value)
    return new_properties


def compose_material(nist_manager, name: str, density: float, components_dict: Dict[str, float]) -> G4Material:
    """Create material from fractional combinations of other materials."""
    material = G4Material(name, density, len(components_dict))
    for elem, fraction in components_dict.items():
        mat = nist_manager.FindOrBuildMaterial(elem)
        assert mat
        material.AddMaterial(mat, fraction)
    return material
