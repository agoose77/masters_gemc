from pathlib import Path
from typing import Any, Dict, Union, Type

import numpy as np
from Geant4 import (G4ThreeVector, mm, eV, G4MaterialPropertiesTable, G4OpticalSurface,
                    G4OpticalSurfaceFinish, G4OpticalSurfaceModel, G4SurfaceType, G4LogicalBorderSurface,
                    G4LogicalSkinSurface, MeV, cm, G4Material, cm3, ns, g, G4RotationMatrix, deg, G4VisAttributes,
                    G4Color, G4SDManager, G4EmLivermorePhysics, G4EmPenelopePhysics, G4EmLowEPPhysics, G4OpticalPhysics,
                    G4HadronPhysicsFTFP_BERT, G4DecayPhysics, G4RadioactiveDecayPhysics, G4VPhysicsConstructor)
from detector import getHCFromVirtual, countOfType, getEDepArray, getPDGArray, SensitiveDetector, getProcessData

PARENT_DIRECTORY = Path(__file__).parent
INTENSITIES_PATH = PARENT_DIRECTORY / "data/naitl_intensity.csv"

NUM_LAYERS: int = None
BEAM_ENERGY: float = None
EM_PHYSICS_CONSTRUCTOR: Type[G4VPhysicsConstructor] = None

NAME_TO_CONSTRUCTOR = {
    'liv': G4EmLivermorePhysics,
    'pen': G4EmPenelopePhysics,
    'emlow': G4EmLowEPPhysics
}


def configure_simulation(energy: float, physics: str = 'emlow', intensities: Path = None):
    global BEAM_ENERGY, EM_PHYSICS_CONSTRUCTOR, INTENSITIES_PATH

    BEAM_ENERGY = energy
    EM_PHYSICS_CONSTRUCTOR = NAME_TO_CONSTRUCTOR[physics]

    if intensities is not None:
        INTENSITIES_PATH = intensities


def get_detection_result(event) -> Union[Dict[str, Any], None]:
    hc_of_this_event = event.GetHCofThisEvent()
    assert hc_of_this_event

    vhc_photocathode = get_hit_collection(hc_of_this_event, "Photocathode")
    vhc_crystal = get_hit_collection(hc_of_this_event, "Crystal")

    hc_crystal = getHCFromVirtual(vhc_crystal)
    not_photons = getPDGArray(hc_crystal) != 0
    e_dep = getEDepArray(hc_crystal)[not_photons].sum()

    hc_photocathode = getHCFromVirtual(vhc_photocathode)
    photocathode_hits = countOfType(hc_photocathode, "opticalphoton")

    if not photocathode_hits:
        return None

    return {'photocathode': photocathode_hits, 'crystal': e_dep}


def define_detectors(detector_builder):
    detector_world = detector_builder.create_box("World", 30 * cm, 80 * cm, 30 * cm, "G4_AIR", logical_mother=None,
                                                 attributes=G4VisAttributes.GetInvisible())

    dp_detector = G4ThreeVector(0.0, 26.5, 5.1 - 8.1915 / 2.0) * cm
    build_ortec_905_4(detector_builder, detector_world, dp_detector)


def define_physics_constructors(physics_list):
    optical = G4OpticalPhysics()
    optical.SetWLSTimeProfile("delta")
    yield optical
    yield G4HadronPhysicsFTFP_BERT()
    yield EM_PHYSICS_CONSTRUCTOR()
    yield G4DecayPhysics()
    yield G4RadioactiveDecayPhysics()
    physics_list.SetVerboseLevel(0)


def define_materials(nist_manager):
    air = nist_manager.FindOrBuildMaterial("G4_AIR")

    photon_energies_single = [1.12713 * eV]
    mpt_air = G4MaterialPropertiesTable()
    mpt_air.AddProperty("RINDEX", photon_energies_single, [1.000293])
    mpt_air.AddProperty("ABSLENGTH", photon_energies_single, [10 * cm])
    air.SetMaterialPropertiesTable(mpt_air)

    # Borosilicate window glass
    borosilicate_glass = compose_material(nist_manager, "Borosilicate glass", 2.23 * g / cm3,
                                          {'G4_B': 0.040064, 'G4_O': 0.539562, 'G4_Na': 0.028191, 'G4_Al': 0.011644,
                                           'G4_Si': 0.377220, 'G4_K': 0.003321})
    borosilicate_glass_properties = G4MaterialPropertiesTable()
    borosilicate_glass_properties.AddProperty("RINDEX", photon_energies_single, [1.49])
    borosilicate_glass.SetMaterialPropertiesTable(borosilicate_glass_properties)

    # Bialkali PMT material
    bialkali = compose_material(nist_manager, "Bialkali", 4.28 * g / cm3,
                                {'G4_K': 0.133, 'G4_Cs': 0.452, 'G4_Sb': 0.415})

    # Scintillator material
    nai_tl_density_g_cm3 = 3.67
    sodium_iodide_tl = compose_material(nist_manager, "NaI(Tl)", nai_tl_density_g_cm3 * g / cm3,
                                        {'G4_SODIUM_IODIDE': 0.996, 'G4_Tl': 0.004})  # [1]

    # Define scintillator material properties table
    scintillator_properties = G4MaterialPropertiesTable()

    # [0] Load intensity profile
    intensity_wavelengths_nm, intensities = np.loadtxt(INTENSITIES_PATH, delimiter=',').T
    energies_intensity = (nm_to_ev(intensity_wavelengths_nm) * eV).tolist()
    intensities = intensities.tolist()

    assert energies_intensity
    assert intensities
    assert all(x >= 0 for x in intensities)
    assert all(x > 0 for x in energies_intensity)
    # [0] Load attenuation
    # k_wavelengths_nm, k_cm_recip = np.loadtxt(PARENT_DIRECTORY / "data/naitl_k.csv", delimiter=',').T
    # energies_attenuation = (nm_to_ev(k_wavelengths_nm) * eV)[::-1].tolist()
    # absorption_lengths = (cm / k_cm_recip * 1000)[::-1].tolist()

    # TODO, is this interplaying with the refractive indices to cause more reflection of certain energy
    scintillator_properties.AddProperty("FASTCOMPONENT", energies_intensity, intensities)
    scintillator_properties.AddProperty("ABSLENGTH", [energies_intensity[0]], [2000 * cm])  # [2]
    scintillator_properties.AddProperty("RINDEX", [energies_intensity[0]], [1.85])  # [1]
    scintillator_properties.AddProperty("EFFICIENCY", [energies_intensity[0]], [0.0])
    scintillator_properties.AddConstProperty("SCINTILLATIONYIELD", 5000 / MeV)  # [1] 38/kev
    scintillator_properties.AddConstProperty("FASTTIMECONSTANT", 28.23 * ns)  # [5]
    scintillator_properties.AddConstProperty("YIELDRATIO", 1.0)
    scintillator_properties.AddConstProperty("RESOLUTIONSCALE", 1.0)
    # ion = sodium_iodide_tl.GetIonisation()
    # ion.SetBirksConstant(3.8e-3 * mm / MeV)  # [6]
    sodium_iodide_tl.SetMaterialPropertiesTable(scintillator_properties)
    # http://oftankonyv.reak.bme.hu/tiki-index.php?page=SPECT+Scintillators


def configure_particle_gun(particle_gun):
    particle_gun.SetParticleByName("gamma")
    particle_gun.SetParticleEnergy(
        BEAM_ENERGY * MeV)  # VERY important, seems default has energy which leads to wrong processes
    particle_gun.SetParticlePosition(G4ThreeVector(0.0, 0.0, 0.0) * mm)
    particle_gun.SetParticleMomentumDirection(G4ThreeVector(0., 10., 0.))


# User routines
def get_hit_collection(hc_of_this_event, sensitive_detector_name: str):
    sd_manager = G4SDManager.GetSDMpointer()
    hc_table = sd_manager.GetHCtable()
    hc_path = f"{sensitive_detector_name}/{sensitive_detector_name}"
    collection_id = hc_table.GetCollectionID(hc_path)
    return hc_of_this_event.GetHC(collection_id)


def nm_to_ev(wavelength):
    """E=hf, f=c/λ, E=hc/λ, V=hc/(λq)"""
    c = 299_792_458
    _q = 1.60217662
    _h = 6.62607004
    return (_h * c * 1e-6) / (wavelength * _q)


def build_ortec_905_4(detector_builder, detector_world, displacement: G4ThreeVector):
    length_crystal = 76.2 * mm
    radius_crystal = 76.2 * mm / 2.0
    length_pmt = 2 * mm
    thickness_wall = 0.508 * mm
    radius_wall = 81.915 * mm / 2.0  # (81.915)/2-.508

    rotation = G4RotationMatrix()
    rotation.rotateX(90 * deg)
    globals()['c'] = rotation

    translation = G4ThreeVector(0.0, (length_crystal + length_pmt + thickness_wall) / 2, 0.0) + \
                  displacement  # Translation rel to source

    detector_detector = detector_builder.create_cylinder("Detector", 0.0, radius_wall,
                                                         length_crystal + length_pmt + thickness_wall, "G4_AIR",
                                                         detector_world.logical,
                                                         rotation=rotation,
                                                         translation=translation,
                                                         attributes=G4VisAttributes(G4Color(0, 0, 1, .4)))

    detector_builder.create_cylinder("Wall", radius_wall - thickness_wall, radius_wall,
                                     length_crystal + length_pmt, 'G4_Al', detector_detector.logical,
                                     translation=G4ThreeVector(0, 0, +thickness_wall / 2),
                                     attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_wall_cap = detector_builder.create_cylinder("WallCap", 0.0, radius_wall, thickness_wall, 'G4_Al',
                                                         detector_detector.logical,
                                                         translation=G4ThreeVector(0, 0.0,
                                                                                   -(length_pmt + length_crystal) / 2),
                                                         attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_crystal = detector_builder.create_cylinder("Crystal", 0.0, radius_crystal, length_crystal, 'NaI(Tl)',
                                                        detector_detector.logical,
                                                        translation=G4ThreeVector(0.0, 0.0,
                                                                                  -(length_pmt - thickness_wall) / 2))
    detector_photocathode = detector_builder.create_cylinder("Photocathode", 0.0, radius_crystal, length_pmt,
                                                             "Bialkali",
                                                             detector_detector.logical,
                                                             translation=G4ThreeVector(0.0, 0.0, (
                                                                 thickness_wall + length_crystal) / 2),
                                                             attributes=G4VisAttributes(G4Color(1, 0, 0)))

    detector_photocathode.make_sensitive(SensitiveDetector("Photocathode"))
    detector_crystal.make_sensitive(SensitiveDetector("Crystal"))

    # Wrapping
    sigma_alpha = 0.23  # https://www.osapublishing.org/DirectPDFAccess/1C3D7D15-B964-614D-AF8B42428CC26C47_210150/oe-19-5-4199.pdf?da=1&id=210150&seq=0&mobile=no
    wrap_surface = G4OpticalSurface("Tyvek", G4OpticalSurfaceModel.unified, G4OpticalSurfaceFinish.groundtyvekair,
                                    G4SurfaceType.dielectric_metal, sigma_alpha)

    mpt_surface = G4MaterialPropertiesTable()
    surface_photon_energies = [1.12713 * eV]
    mpt_surface.AddProperty("REFLECTIVITY", surface_photon_energies, [0.975])
    mpt_surface.AddProperty("EFFICIENCY", surface_photon_energies, [0])
    wrap_surface.SetMaterialPropertiesTable(mpt_surface)
    G4LogicalBorderSurface("Tyvek", detector_crystal.physical, detector_detector.physical, wrap_surface)
    G4LogicalBorderSurface("Tyvek Cap", detector_crystal.physical, detector_wall_cap.physical, wrap_surface)

    # Photocathode optical properties
    photocathode_surface = G4OpticalSurface("PMTSurface", G4OpticalSurfaceModel.unified,
                                            G4OpticalSurfaceFinish.polished, G4SurfaceType.dielectric_metal)
    photon_energies_PMT = [5.0 * eV]
    mpt_photocathode = G4MaterialPropertiesTable()

    mpt_photocathode.AddProperty("RINDEX", photon_energies_PMT, [1.5])
    mpt_photocathode.AddProperty("EFFICIENCY", photon_energies_PMT, [1.0])
    mpt_photocathode.AddProperty("REFLECTIVITY", photon_energies_PMT, [0.0])
    mpt_photocathode.AddProperty("ABSLENGTH", photon_energies_PMT, [1e-4 * mm])
    photocathode_surface.SetMaterialPropertiesTable(mpt_photocathode)
    G4LogicalSkinSurface("PhotocathodeSkin", detector_photocathode.logical, photocathode_surface)


def compose_material(nist_manager, name: str, density: float, components_dict: Dict[str, float]) -> G4Material:
    """Create material from fractional combinations of other materials."""
    material = G4Material(name, density, len(components_dict))
    for elem, fraction in components_dict.items():
        mat = nist_manager.FindOrBuildMaterial(elem)
        assert mat
        material.AddMaterial(mat, fraction)
    return material
