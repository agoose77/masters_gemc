import numpy as np

# SNR = mean / stddev
K: int = 10 # Require that 3 <= 7 <= 10, taken from ETI9305 specification
DELTA: float = 0.7e6 ** 0.1 # Dynode gain^k = G (G=0.7e6 from ETI9506)
CHARGE_MAX: float = 1.3*38_000*0.27*DELTA**K # Max channel manually determined by given charge


def sigma_exp(npe: np.ndarray, k: float, delta: float) -> np.ndarray:
    return np.sqrt(npe * delta**(2*k) * (1+(delta+1)/(delta-1)))


def sigma_poisson(npe: np.ndarray, k: float, delta: float) -> np.ndarray:
    return np.sqrt(npe * delta ** (2*k) * (1+1/(delta-1)))


def pmt(nph: np.ndarray, sigma_func=sigma_exp) -> np.ndarray:
    npe = nph.astype(np.int) #G-63,p168
    sigma = sigma_func(npe, K, DELTA)
    gain = DELTA ** K
    ne_anode_mean = npe * gain
    return np.random.normal(ne_anode_mean, sigma).astype(np.int_).clip(0, None) # TODO how should this be dealt with?


def adc(charge: np.ndarray, num_channels: int, charge_max: float=CHARGE_MAX) -> np.ndarray:
    return ((charge / charge_max) * (num_channels-1)).astype(np.int_)


def nph_to_adc(nph: np.ndarray, num_channels: int, charge_max: float=CHARGE_MAX, sigma_func=sigma_exp):
    charge = pmt(nph, sigma_func)
    return adc(charge, num_channels, charge_max)