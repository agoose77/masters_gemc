import sys; sys.path.append("../../")
from analysis.runs import load_run, Run, aggregate_runs
from pathlib import Path
from collections import defaultdict
from argument_parser import ShieldingInfo, parse_shielding
from typing import NamedTuple, Dict

    
class TemplateInfo(NamedTuple):
    source: str
    shielding: ShieldingInfo = None
        
    def __str__(self):
        return f"{self.source} source, {self.shielding if self.shielding else 'no'} shielding"
    
    
def template_from_run(run: Run) -> TemplateInfo:
    info = run.run_info
    kwargs = info.specification_kwargs
    shielding = parse_shielding(kwargs['shielding']) if kwargs.get('shielding') else None
    return TemplateInfo(kwargs['source'], shielding)


def load_templates(directory: Path) -> Dict[TemplateInfo, Run]:
    runs = [load_run(p) for p in directory.iterdir() if not p.name.startswith(".") and len([*p.iterdir()])>1]
    template_to_runs = {}
    
    for run in runs:
        template = template_from_run(run)
        if template in template_to_runs:
            existing = template_to_runs[template]
            run = aggregate_runs(existing, run, ignore_compatability=True)
            
        template_to_runs[template] = run
        
    return template_to_runs