from analysis.io import load_spe, hist_from_spe
from analysis.utils import glob_regex
from analysis.spectrum import ensemble_average
from collections import defaultdict
from typing import NamedTuple, Tuple, Dict
from pathlib import Path
import numpy as np


SPECTRA_DIR = Path.cwd() / "spectra"


class ExperimentInfo(NamedTuple):
    source: str
    layers: int
        
    def __str__(self):
        return f"{self.source}, {self.layers} shielding layers"

    
def load_bg_histogram() -> Tuple[np.ndarray, np.ndarray]:
    return ensemble_average([hist_from_spe(load_spe(p)) 
                             for p in SPECTRA_DIR.glob('bg*.Spe')])
        
        
def load_experimental_histograms() -> Dict[ExperimentInfo, Tuple[np.ndarray, np.ndarray]]:
    # Group all experiments
    expt_info_to_hist = defaultdict(list)
    for m, p in glob_regex(SPECTRA_DIR, f"(.*)_(\d)_shield.*\.Spe"):
        hist = hist_from_spe(load_spe(p))
        
        info = ExperimentInfo(m.group(1), int(m.group(2)))
        expt_info_to_hist[info].append(hist)
        
    return {e: ensemble_average(expt_info_to_hist[e]) for e in sorted(expt_info_to_hist)}