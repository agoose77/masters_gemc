from typing import NamedTuple
from re import compile


class ShieldingInfo(NamedTuple):
    material: str
    thickness_mm: float
        
    def __str__(self):
        return f"{self.thickness_mm} mm of {self.material}"
    
    
_shield_pattern = compile(r"^(?P<thickness>[-+]?(?:[1-9][0-9]*)?\.?[0-9]+)(?P<unit>[a-z][a-z]?) (?P<material>\w+)$")
def parse_shielding(shielding: str) -> ShieldingInfo:
    match = _shield_pattern.match(shielding)
    if match is None:
        raise ValueError(shielding)

    length, unit, material = match.groups(('thickness', 'unit', 'material'))
    name_to_unit = {'mm': 1, 'm': 1e3, 'cm': 1e2}
    return ShieldingInfo(material, float(length) * name_to_unit[unit])
