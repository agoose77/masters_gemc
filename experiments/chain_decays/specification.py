from pathlib import Path
from re import compile, IGNORECASE
from typing import Dict, Union, Any, Tuple, NamedTuple

import numpy as np
from Geant4 import (G4ThreeVector, m, mm, eV, keV, rad, G4MaterialPropertiesTable, G4OpticalSurface,
                    G4OpticalSurfaceFinish, G4OpticalSurfaceModel, G4SurfaceType, G4LogicalBorderSurface,
                    G4LogicalSkinSurface, G4ParticleTable, MeV, cm, G4Material, cm3, ns, g, G4RotationMatrix, deg,
                    G4VisAttributes, G4Color, G4SDManager)
from detector import getHCFromVirtual, countOfType, getEDepArray, getPDGArray, SensitiveDetector, TrackingAction
from mendeleev import element
from argument_parser import ShieldingInfo, parse_shielding

PARENT_DIRECTORY = Path(__file__).parent


class SourceInfo(NamedTuple):
    protons: int
    nucleons: int


SHIELD: ShieldingInfo = None
SOURCE: SourceInfo = None
CHAIN_LEAF_ISOTOPE: SourceInfo = None
NO_SCINTILLATION: bool = False
    

def define_actions(run_manager):
    if CHAIN_LEAF_ISOTOPE is not None:
        print(f"Using tracking action to prevent decays of {CHAIN_LEAF_ISOTOPE}")
        yield TrackingAction(CHAIN_LEAF_ISOTOPE.nucleons, CHAIN_LEAF_ISOTOPE.protons)


def get_detection_result(event) -> Union[Dict[str, Any], None]:
    hc_of_this_event = event.GetHCofThisEvent()
    assert hc_of_this_event
    
    vhc_crystal = get_hit_collection(hc_of_this_event, "Crystal")
    hc_crystal = getHCFromVirtual(vhc_crystal)
    not_photons = getPDGArray(hc_crystal) != 0
    e_dep = getEDepArray(hc_crystal)[not_photons].sum()

    if not e_dep:
        return None

    result = {'crystal': e_dep}
    
    if not NO_SCINTILLATION:
        vhc_photocathode = get_hit_collection(hc_of_this_event, "Photocathode")
        hc_photocathode = getHCFromVirtual(vhc_photocathode)
        photocathode_hits = countOfType(hc_photocathode, "opticalphoton")
        result['photocathode'] = photocathode_hits
        
    return result


def configure_simulation(source: str, shielding: str = None, chain_leaf_isotope: str=None, no_scintillation: bool=False):
    global SHIELD, SOURCE, CHAIN_LEAF_ISOTOPE, NO_SCINTILLATION
    if shielding is not None:
        SHIELD = parse_shielding(shielding)
        
    if chain_leaf_isotope is not None:
        CHAIN_LEAF_ISOTOPE = parse_source_info(chain_leaf_isotope)

    SOURCE = parse_source_info(source)
    NO_SCINTILLATION = no_scintillation


def define_detectors(detector_builder):
    detector_world = detector_builder.create_box("World", 30 * cm, 80 * cm, 30 * cm, "G4_AIR", logical_mother=None,
                                                 attributes=G4VisAttributes.GetInvisible())

    #dp_coffin = G4ThreeVector(0.0, 16.5, 0.0) * cm
    #build_coffin(detector_builder, detector_world, dp_coffin, SHIELD)

    # 26.5 from 20
    dp_detector = G4ThreeVector(0.0, 26.5, 5.1 - 8.1915 / 2.0) * cm
    build_ortec_905_4(detector_builder, detector_world, dp_detector)


def define_materials(nist_manager):
    air = nist_manager.FindOrBuildMaterial("G4_AIR")

    # These properties are incorrect, but never expected to be in air volume for more than a fraction of a mm.
    # Probably can remove
    photon_energies_single = [1.12713 * eV]
    mpt_air = G4MaterialPropertiesTable()
    mpt_air.AddProperty("RINDEX", photon_energies_single, [1.000293])
    mpt_air.AddProperty("ABSLENGTH", photon_energies_single, [10 * cm])
    air.SetMaterialPropertiesTable(mpt_air)

    # Borosilicate window glass
    borosilicate_glass = compose_material(nist_manager, "Borosilicate glass", 2.23 * g / cm3,
                                          {'G4_B': 0.040064, 'G4_O': 0.539562, 'G4_Na': 0.028191, 'G4_Al': 0.011644,
                                           'G4_Si': 0.377220, 'G4_K': 0.003321})
    borosilicate_glass_properties = G4MaterialPropertiesTable()
    borosilicate_glass_properties.AddProperty("RINDEX", photon_energies_single, [1.49])
    borosilicate_glass.SetMaterialPropertiesTable(borosilicate_glass_properties)

    # Bialkali PMT material
    bialkali = compose_material(nist_manager, "Bialkali", 4.28 * g / cm3,
                                {'G4_K': 0.133, 'G4_Cs': 0.452, 'G4_Sb': 0.415})
    bialkali_properties = G4MaterialPropertiesTable()
    bialkali_properties.AddProperty("RINDEX", photon_energies_single, [1.5])
    bialkali_properties.AddProperty("ABSLENGTH", photon_energies_single, [1e-4 * mm])
    bialkali.SetMaterialPropertiesTable(bialkali_properties)

    # Scintillator material
    nai_tl_density_g_cm3 = 3.67
    sodium_iodide_tl = compose_material(nist_manager, "NaI(Tl)", nai_tl_density_g_cm3 * g / cm3,
                                        {'G4_SODIUM_IODIDE': 0.996, 'G4_Tl': 0.004})  # [1]

    # Define scintillator material properties table
    scintillator_properties = G4MaterialPropertiesTable()

    # [0] Load intensity profile
    energies_intensity, intensities = load_data_from_csv_nm(PARENT_DIRECTORY / "data/naitl_intensity.csv")
    if not NO_SCINTILLATION:
        scintillator_properties.AddProperty("FASTCOMPONENT", energies_intensity, intensities)
        scintillator_properties.AddConstProperty("SCINTILLATIONYIELD", 38 / keV)  # [1] 38/kev
        scintillator_properties.AddProperty("ABSLENGTH", [energies_intensity[0]], [1000 * cm])  # [2]
        scintillator_properties.AddProperty("RINDEX", [energies_intensity[0]], [1.85])  # [1]
        scintillator_properties.AddProperty("EFFICIENCY", [energies_intensity[0]], [0.0])
        scintillator_properties.AddConstProperty("FASTTIMECONSTANT", 28.23 * ns)  # [5]
        scintillator_properties.AddConstProperty("YIELDRATIO", 1.0)
        scintillator_properties.AddConstProperty("RESOLUTIONSCALE", 1.0)

    ion = sodium_iodide_tl.GetIonisation()
    ion.SetBirksConstant(3.8e-3 * mm / MeV)  # [6]

    sodium_iodide_tl.SetMaterialPropertiesTable(scintillator_properties)
    # http://oftankonyv.reak.bme.hu/tiki-index.php?page=SPECT+Scintillators


def configure_particle_gun(particle_gun):
    ions = G4ParticleTable.GetParticleTable().GetIonTable()

    particle_gun.SetParticleDefinition(ions.GetIon(SOURCE.protons, SOURCE.nucleons))
    particle_gun.SetParticleEnergy(0 * MeV)  # VERY important, seems default has energy which leads to wrong processes
    particle_gun.SetParticlePosition(G4ThreeVector(0.0, 0.0, 0.0) * mm)
    particle_gun.SetParticleMomentumDirection(G4ThreeVector(0., 10., 0.))


# User routines
def build_coffin(detector_builder, detector_world: 'Detector', displacement: G4ThreeVector,
                 shielding: ShieldingInfo = None):
    vis_attrs = G4VisAttributes(G4Color(1, 0, 0, 0.3))

    detector_builder.create_box("Coffin Floor", 20 * cm, 50 * cm, 5 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs, translation=G4ThreeVector(0.0, -5.0, -7.5) * cm + displacement)
    detector_builder.create_box("Coffin Roof Middle", 20 * cm, 20 * cm, 5 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs, translation=G4ThreeVector(0.0, 0.0, 7.5) * cm + displacement)
    detector_builder.create_box("Coffin End", 20 * cm, 5 * cm, 10 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs, translation=G4ThreeVector(0.0, -22.5, 0.0) * cm + displacement)
    detector_builder.create_box("Coffin Front Left", 5 * cm, 20 * cm, 10 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector((-21.5 + 5) / 2, 10, 0) * cm + displacement)
    detector_builder.create_box("Coffin Front Right", 5 * cm, 20 * cm, 10 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector((+21.5 - 5) / 2, 10, 0) * cm + displacement)
    shield_thickness = 0.0

    if shielding is not None:
        shield_material = shielding.material
        shield_thickness = shielding.thickness_mm * mm
        detector_builder.create_box(f"Coffin Slab", 11 * cm, shield_thickness, 11 * cm, shield_material,
                                    detector_world.logical, attributes=vis_attrs,
                                    translation=G4ThreeVector(0.0,
                                                              -(20 * cm + shield_thickness) / 2,
                                                              0.5 * cm
                                                              ) + displacement)
    detector_builder.create_box("Coffin Roof End", 20 * cm, 10 * cm, 5 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector(0.0, -(15 * cm + shield_thickness),
                                                          7.5 * cm) + displacement)
    detector_builder.create_box("Coffin Middle Left", 5 * cm, 20 * cm, 10 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector(-(11 + 5) / 2, -10, 0) * cm + displacement)
    detector_builder.create_box("Coffin Middle Right", 5 * cm, 20 * cm, 10 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector((11 + 5) / 2, -10, 0) * cm + displacement)
    detector_builder.create_box("Spacer Left", 7 * cm, 7.5 * cm, 5 * mm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector(-(11.5 + 7) / 2, 10 + 7.5 / 2, 5.25) * cm + displacement)
    detector_builder.create_box("Spacer Right", 7 * cm, 7.5 * cm, 5 * mm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector((11.5 + 7) / 2, 10 + 7.5 / 2, 5.25) * cm + displacement)
    detector_builder.create_box("Coffin Roof Front", 20 * cm, 10 * cm, 5 * cm, "G4_Pb", detector_world.logical,
                                attributes=vis_attrs,
                                translation=G4ThreeVector(0.0, 15.0, 7.5 + .5) * cm + displacement)


def build_ortec_905_4(detector_builder, detector_world, displacement: G4ThreeVector):
    length_crystal = 76.2 * mm
    radius_crystal = 76.2 * mm / 2.0
    length_pmt = 2 * mm
    thickness_wall = 1.0 * mm #0.508 * mm
    radius_wall = 81.915 * mm / 2.0  # (81.915)/2-.508

    rotation = G4RotationMatrix()
    rotation.rotateX(90 * deg)

    # Origin of volumes at COM, so displace by COM and then source offset
    translation = G4ThreeVector(0.0, (length_crystal + length_pmt + thickness_wall) / 2, 0.0) + \
                  displacement  # Translation rel to source

    detector_detector = detector_builder.create_cylinder("Detector", 0.0, radius_wall,
                                                         length_crystal + length_pmt + thickness_wall, "G4_AIR",
                                                         detector_world.logical,
                                                         rotation=rotation,
                                                         translation=translation,
                                                         attributes=G4VisAttributes(G4Color(0, 0, 1, .4)))

    detector_builder.create_cylinder("Wall", radius_wall - thickness_wall, radius_wall,
                                     length_crystal + length_pmt, 'G4_Al', detector_detector.logical,
                                     translation=G4ThreeVector(0, 0, +thickness_wall / 2),
                                     attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_wall_cap = detector_builder.create_cylinder("WallCap", 0.0, radius_wall, thickness_wall, 'G4_Al',
                                                         detector_detector.logical,
                                                         translation=G4ThreeVector(0, 0.0,
                                                                                   -(length_pmt + length_crystal) / 2),
                                                         attributes=G4VisAttributes(G4Color(0, 1, 0, .4)))
    detector_crystal = detector_builder.create_cylinder("Crystal", 0.0, radius_crystal, length_crystal, 'NaI(Tl)',
                                                        detector_detector.logical,
                                                        translation=G4ThreeVector(0.0, 0.0,
                                                                                  -(length_pmt - thickness_wall) / 2))
    detector_photocathode = detector_builder.create_cylinder("Photocathode", 0.0, radius_crystal, length_pmt,
                                                             "Bialkali",
                                                             detector_detector.logical,
                                                             translation=G4ThreeVector(0.0, 0.0, (
                                                                 thickness_wall + length_crystal) / 2),
                                                             attributes=G4VisAttributes(G4Color(1, 0, 0)))

    if not NO_SCINTILLATION:
        detector_photocathode.make_sensitive(SensitiveDetector("Photocathode"))
    detector_crystal.make_sensitive(SensitiveDetector("Crystal"))

    # Wrapping [9]
    # https://www.osapublishing.org/DirectPDFAccess/1C3D7D15-B964-614D-AF8B42428CC26C47_210150/oe-19-5-4199.pdf?da=1&id=210150&seq=0&mobile=no
    wrap_surface = G4OpticalSurface("PTFE", G4OpticalSurfaceModel.unified,
                                    G4OpticalSurfaceFinish.groundbackpainted,
                                    G4SurfaceType.dielectric_dielectric, 0.23 * rad)

    # load refractive indices
    rindex_energies, r_indices = load_data_from_csv_nm(PARENT_DIRECTORY / "data/rindex_teflon.csv")
    reflectivity_energies, reflectivities = load_data_from_csv_nm(PARENT_DIRECTORY / "data/refl_teflon.csv")

    teflon_properties = G4MaterialPropertiesTable()
    # No specular, poor approximation [10]
    teflon_properties.AddProperty("REFLECTIVITY", reflectivity_energies, reflectivities)  # [10]
    teflon_properties.AddProperty("RINDEX", rindex_energies, r_indices)
    teflon_properties.AddProperty("SPECULARLOBECONSTANT", rindex_energies, np.ones_like(rindex_energies))
    teflon_properties.AddProperty("SPECULARSPIKECONSTANT", rindex_energies, np.zeros_like(rindex_energies))
    teflon_properties.AddProperty("BACKSCATTERCONSTANT", rindex_energies, np.zeros_like(rindex_energies))
    wrap_surface.SetMaterialPropertiesTable(teflon_properties)

    # surface_photon_energies = [1.12713 * eV]
    # teflon_properties.AddProperty("REFLECTIVITY", surface_photon_energies, [0.992]) #[8]

    G4LogicalBorderSurface("PTFE Wall", detector_crystal.physical, detector_detector.physical, wrap_surface)
    G4LogicalBorderSurface("PTFE Cap", detector_crystal.physical, detector_wall_cap.physical, wrap_surface)

    # Photocathode optical properties
    photocathode_surface = G4OpticalSurface("PhotocathodeSurface", G4OpticalSurfaceModel.unified,
                                            G4OpticalSurfaceFinish.polished, G4SurfaceType.dielectric_metal)
    eneriges_photocathode = [5.0 * eV]

    # load efficiencies
    efficiency_energies, efficiencies_pct = load_data_from_csv_nm(PARENT_DIRECTORY / "data/qe_pmt_eti9305.csv")

    bialkali_properties = G4Material.GetMaterial('Bialkali').GetMaterialPropertiesTable()
    photocathode_properties = copy_material_properties(bialkali_properties)
    photocathode_properties.AddProperty("EFFICIENCY", efficiency_energies, efficiencies_pct * 1e-2)
    photocathode_properties.AddProperty("REFLECTIVITY", eneriges_photocathode, [0.0])
    photocathode_surface.SetMaterialPropertiesTable(photocathode_properties)
    G4LogicalSkinSurface("PhotocathodeSkin", detector_photocathode.logical, photocathode_surface)


def define_physics_cuts(physics_list) -> Dict[str, float]:
    default_cut_value = 4 * mm
    return {'gamma': default_cut_value,
            'e-': default_cut_value,
            'e+': default_cut_value,
            'proton': default_cut_value}


def nm_to_ev(wavelength):
    """E=hf, f=c/λ, E=hc/λ, V=hc/(λq)"""
    c = 299_792_458
    _q = 1.60217662
    _h = 6.62607004
    return (_h * c * 1e-6) / (wavelength * _q)


def parse_source_info(name: str) -> SourceInfo:
    pattern = compile(r"^(?P<name>[^\d]+)(?P<nucleons>\d+)$", flags=IGNORECASE)
    match = pattern.match(name)
    assert match is not None
    groups = match.groupdict()

    n_protons = element(groups['name']).protons
    n_nucleons = int(groups['nucleons'])

    return SourceInfo(n_protons, n_nucleons)


def copy_material_properties(properties):
    new_properties = G4MaterialPropertiesTable()
    for name, vec in properties.GetPropertiesMap().items():
        new_properties.AddProperty(str(name), vec)
    for name, value in properties.GetPropertiesCMap().items():
        new_properties.AddConstProperty(str(name), value)
    return new_properties


def compose_material(nist_manager, name: str, density: float, components_dict: Dict[str, float]) -> G4Material:
    """Create material from fractional combinations of other materials."""
    material = G4Material(name, density, len(components_dict))
    for elem, fraction in components_dict.items():
        mat = nist_manager.FindOrBuildMaterial(elem)
        assert mat
        material.AddMaterial(mat, fraction)
    return material


def load_data_from_csv_nm(path: Path) -> Tuple[np.ndarray, np.ndarray]:
    wavelengths_nm, values = np.loadtxt(path, delimiter=',').T
    value_energies = nm_to_ev(wavelengths_nm) * eV
    return value_energies, values


# User routines
def get_hit_collection(hc_of_this_event, sensitive_detector_name: str):
    sd_manager = G4SDManager.GetSDMpointer()
    hc_table = sd_manager.GetHCtable()
    hc_path = f"{sensitive_detector_name}/{sensitive_detector_name}"
    collection_id = hc_table.GetCollectionID(hc_path)
    return hc_of_this_event.GetHC(collection_id)

# references
# [0] https://openi.nlm.nih.gov/detailedresult.php?img=PMC3069330_10439_2011_266_Fig6_HTML&req=4
# [1] https://www.crystals.saint-gobain.com/products/nai-sodium-iodide
# [2] https://wiki.nikhef.nl/detector/pub/Main/ArticlesAndTalks/Master_Thesis_PA_Breur.pdf
# [3] http://www.jocpr.com/articles/measurement-of-attenuation-coefficient-and-mean-free-path-of-some-vitamins-in-the-energy-range-01221330-mev.pdf
# [4] https://link-springer-com.ezproxyd.bham.ac.uk/content/pdf/10.1023%2FA%3A1022422823022.pdf (OPTICAL AND SCINTILLATION PROPERTIES OF NaI(Tl) CRYSTALS)
# [5] https://arxiv.org/pdf/1307.1398.pdf
# [6] http://www.aesj.or.jp/publication/pnst001/data/218.pdf
# [7] http://flashpointcrystals.com/attachments/article/65/Montecarlo%20method%20for%20determining%20absolute%20Scintillation%20Photon%20Yields.pdf
# [8] https://escholarship.org/content/qt82r9k9rn/qt82r9k9rn.pdf
# [9] https://www.psi.ch/mu3e/ThesesEN/BachelorBaumgartner.pdf
# [10] https://arxiv.org/pdf/0910.1056.pdf
# [11] https://arxiv.org/pdf/0907.2771.pdf diffuse reflector use backpaint
