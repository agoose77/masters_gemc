# GEANT4 Synthetic Generation of Spectra, and Radioisotope Identification

This repository contains the source files used to generate synthetic gamma spectra for an Ortec 905-4 NaI(Tl) scintillation detector, and subsequent analysis routines for identifying radioisotopic components.

This project is built upon Python3.6, for use of its async support, f-strings, and type annotations. The following libraries are also of particular importance:

* The `dispy` python library was used to distribute and manage multi-core simulations across a local network. In this project, a pair of AMD Ryzen 5 1600 (hexa core) and Intel i7 960M (quad core) CPUs were used, providing 20 available threads between them. In future work, projects like `dask.distributed` would perhaps be more robust candidates to use.
* The `bokeh` plotting library was used with the `JupyterLab` Jupyter interface. Jupyter is a powerful framework, whose "notebooks"
    > are documents that combine live runnable code with narrative text (Markdown), equations (LaTeX), images, interactive visualizations and other rich output:
* The `numba` library was used to increase the performance of pure Python numeric code which could not be easily expressed in numpy vectorised operations.

To manage the dependencies of the project, the `pipenv` library was used. Simply install `pipenv`, and run `pipenv install` to install all dependencies. First, you will need "pip" installed (see [requirements-for-installing-packages](https://packaging.python.org/tutorials/installing-packages/#requirements-for-installing-packages))

All Python code (including simulation and analysis) was run from JupyterLab (rather than Jupyter notebook, though the latter may be used instead). Because of the use of `bokeh` rather than `matplotlib`, in many cases plots may fail to load until they're run. I haven't explored why this is, yet.

## Contents
### Macros
In the `macros` directory are a number of macros which were used over the course of the project. Macros beginning with `source_` were used initially before a mechanism existed for configuring the simulation source. 

### Analysis
The `analysis` directory is a Python package which defines a number of tools used frequently during spectrum analysis. Its feature set includes:
* N-Gaussian peak fitting
* ROOT peak finding
* The first 5 order polynomials for calibration
* A OO interface to SPE reading
* SNIP background estimation
* Binomial smoothing
* Plotting API (to minimise boilerplate required to define colours / output images)
* MIMBS isotopic fitting
* X-ray and gamma emission data tables (source unknown)
* API to load run results from simulations

### Experiments
Where possible, each distinct experiment was separated into its own directory. Several experiments depend upon the simulated templates defined in `riid_templates/runs`, and so reference these by relative path in the notebooks. 

1. `chain_decays` - an experiment to test the `G4UserTrackingAction` provided by `geant4_helpers` to cull tracks which follow from the ground state of a particular isotope. Also simple 
1. `specular_constant` - an experiment to investigate the effect of varying the various optical constants.
1. `physics_lists` - a comparison of the different low energy physics constructors for a 137Cs source.
1. `nonlinearity` - an experimental evaluation of the calibration behaviour of the Ortec 905-4 detector over a range of energies
1. `skew_test` - primary investigation into observed skew in the optical photon distributions


## Serving dispy
Dispy can be started from `serve.sh`, which simply loads the dispy server with the appropriate configuration parameters.
