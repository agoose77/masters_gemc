from typing import Tuple

import numpy as np
from scipy.interpolate import interp1d
from scipy.signal import lfilter


def smooth_binomial(y: np.ndarray, n: int) -> np.ndarray:
    # From https://www.mathworks.com/help/signal/examples/signal-smoothing.html
    h = np.array((0.5, 0.5))
    binoms = np.convolve(h, h)

    for i in range(1, n):
        binoms = np.convolve(binoms, h)

    delay = (len(binoms) - 1) / 2
    smoothed = lfilter(binoms, 1, y)

    x = np.arange(y.shape[0])
    f = interp1d(x, smoothed, fill_value=0.0, bounds_error=False)
    return f(x + delay)


def estimate_background(y: np.ndarray, iterations: int) -> np.ndarray:
    """Estimate background using SNIP algorithm"""
    assert iterations > 0
    v = np.log(np.log(np.sqrt(y + 1) + 1) + 1)
    len_v = len(v)

    for m in range(1, iterations + 1):
        average = (v[:len_v - m * 2] + v[m * 2:]) / 2
        v[m:len_v - m] = np.minimum(v[m:len_v - m], average)

    return (np.exp(np.exp(v) - 1) - 1) ** 2 - 1


def rms_diff(a: np.ndarray, b: np.ndarray) -> float:
    assert a.shape == b.shape
    return np.linalg.norm(a - b) / np.sqrt(len(a))


def make_histogram(counts: np.ndarray, n_bins: int, range: Tuple[float, float] = None) -> Tuple[
    np.ndarray, np.ndarray, np.ndarray]:
    y, bin_widths = np.histogram(counts, n_bins, range=range)
    x = (bin_widths[:-1] + bin_widths[1:]) / 2
    return x, y, bin_widths


def rebin_histogram(x: np.ndarray, y: np.ndarray, bin_centroids: np.ndarray, interpolate: bool = True) -> np.ndarray:
    if interpolate:
        return np.interp(bin_centroids, x, y)

    bin_edges = (bin_centroids[:-1] + bin_centroids[1:]) / 2
    new_x = np.digitize(x, bin_edges)
    values = np.zeros(len(bin_centroids), dtype=y.dtype)
    np.add.at(values, new_x, y)
    return values


def ensemble_average(histograms):
    x_arrs, y_arrs = zip(*histograms)
    x = np.array(x_arrs).mean(axis=0)
    y = np.array(y_arrs).mean(axis=0)
    return x, y
