from os import environ
from typing import Tuple, NamedTuple, Union

import numpy as np
from inspect import signature
from scipy.optimize import differential_evolution, least_squares, OptimizeResult, nnls

environ['NUMBA_WARNINGS'] = '1'
import numba


def with_signature_of(other):
    def wrapper(func):
        func.__signature__ = signature(other)
        return func
    return wrapper



class MIMBSFitResult(NamedTuple):
    fit_histogram: np.ndarray
    isotope_density: np.ndarray
    basis_density: np.ndarray
    cost: float


def make_array_with_head(values: np.ndarray, shape: Union[Tuple[int], np.ndarray], dtype: np.dtype = None,
                         trailing=-0.0) -> np.ndarray:
    """Create new array with given shape, with any trailing values given by trailing parameter

    :param values: values to populate array head
    :param shape: array shape
    :param dtype: dtype of array
    :param trailing: value which which to fill remaining entries
    """
    arr = np.empty(shape, dtype=dtype)
    arr[:len(values)] = values
    arr[len(values):] = trailing
    return arr


# # Define MIMBS algorithm
@numba.jit(nopython=True)
def calc_l_b(t: np.ndarray, thickness_matrix: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    """Determine the values for λ_(j,z) and b(j,z)

    :param t: absorber density vector
    :param thickness_matrix: 3d matrix of source, absorber, thickness
    """
    n_sources, n_absorbers, n_thickness_matrix = thickness_matrix.shape
    shape = (n_sources, n_absorbers)
    l = np.empty(shape, dtype=np.float_)
    b = np.empty(shape, dtype=np.int_)

    for j in range(n_sources):
        thickness_matrix_j = thickness_matrix[j]
        for z in range(n_absorbers):
            t_z = t[z]
            t_jz = thickness_matrix_j[z]

            b_jz = 0
            # Should use binary search for large N
            for i in range(len(t_jz)):
                # Found bounding value
                if t_z < t_jz[i]:
                    b_jz = i - 1
                    break

                # Null entry
                elif i > 1 and t_jz[i - 1] > t_jz[i]:
                    b_jz = i - 2
                    break
            else:
                b_jz = i - 1

            l[j, z] = (t_z - t_jz[b_jz]) / (t_jz[b_jz + 1] - t_jz[b_jz])
            b[j, z] = b_jz

    return l, b


@numba.jit(nopython=True)
def R_t(t: np.ndarray, counts_matrix: np.ndarray, thickness_matrix: np.ndarray) -> np.ndarray:
    """Calculate response matrix for given absorber density vector

    :param t: absorber density vector
    :param counts_matrix: 4d matrix of source, absorber, thickness, histogram
    :param thickness_matrix: 3d matrix of source, absorber, thickness
    """
    n_sources, n_absorbers, n_thickness_matrix, n_bins = counts_matrix.shape
    result = np.empty((n_bins, n_sources), dtype=np.float_)

    l, b = calc_l_b(t, thickness_matrix)

    for j in range(n_sources):
        counts_ref_j = counts_matrix[j,0,0]
        counts_template_j = counts_matrix[j]

        l_j = l[j]
        b_j = b[j]

        for i in range(n_bins):
            f = 0.0

            for z in range(n_absorbers):
                counts_jz = counts_template_j[z]

                l_jzb = l_j[z]
                b_jzb = b_j[z]
                
                c_jzb_1 = counts_jz[b_jzb + 1,i]
                c_jzb = counts_jz[b_jzb, i]
                
                # Handles case where c_jzb==c_jzb_1==0 (0/0 situation) and case where
                # extrapolating from 0->1->n(t) counts, as gradient = infinity
                if c_jzb_1 == 0 or c_jzb == 0:
                    df = -np.inf
                else:
                    df = (l_jzb * np.log(c_jzb_1) + (1 - l_jzb) * np.log(c_jzb))
                f += df
            result[i, j] = counts_ref_j[i] ** (1 - n_absorbers) * np.exp(f)

    return result


@numba.jit(nopython=True)
def residuals_given_a_R_sqrt(a: np.ndarray, R: np.ndarray, counts_transform: np.ndarray, low_e_limit_index: int) -> np.ndarray:
    """Return vector of residuals corresponding to sqrt(y) - sqrt(R@a) for channels >= low energy limit """
    return (np.sqrt(R@a) - counts_transform)[low_e_limit_index:]


def residual_for_t_linear(t: np.ndarray, expected_values: np.ndarray, counts_matrix: np.ndarray,
                          thicknesses: np.ndarray, low_e_limit_index: int) -> float:
    """Return rnorm value for scipy.optimize.nnls solution to argmin(R@x-counts)

    :param t: basis density vector
    :param expected_values: test histogram
    :param counts_matrix: 4d matrix of source, absorber, thickness, histogram
    :param thicknesses: 3d matrix of source, absorber, thickness
    :param low_e_limit_index: energy below which to avoid fitting due to k-edge effects
    """
    R = R_t(t, counts_matrix, thicknesses)
    x, rnorm = nnls(R[low_e_limit_index:], expected_values[low_e_limit_index:])
    return rnorm


def fit_for_t(t: np.ndarray, expected_values: np.ndarray, counts_matrix: np.ndarray,
              thicknesses: np.ndarray, bounds_a: np.ndarray, low_e_limit_index: int) -> OptimizeResult:
    """Return scipy.optimize.OptimizeResult fit result determined for a given t

    :param t: basis density vector
    :param expected_values: test histogram transformed into sqrt space
    :param counts_matrix: 4d matrix of source, absorber, thickness, histogram
    :param thicknesses: 3d matrix of source, absorber, thickness
    :param bounds_a: 2 tuple/array of min and max bounds for isotope compositions
    :param low_e_limit_index: energy below which to avoid fitting due to k-edge effects
    """
    R = R_t(t, counts_matrix, thicknesses)
    args = (R, expected_values, low_e_limit_index)
    a0 = np.zeros(counts_matrix.shape[0])
    return least_squares(residuals_given_a_R_sqrt, a0, bounds=bounds_a, args=args)
    

@with_signature_of(fit_for_t)
def fit_residuals_given_t(*args, **kwargs) -> np.ndarray:
    """Return fit residuals for a given t.

    For parameter documentation see `fit_for_t`
    """
    return fit_for_t(*args, **kwargs).fun


@with_signature_of(fit_for_t)
def ssr_given_t(*args, **kwargs) -> float:
    """Return the sum of the squares of the fit residuals for a given t.

    For parameter documentation see `fit_for_t`
    """
    return (fit_residuals_given_t(*args, **kwargs) ** 2).sum()


def estimate_parameters(counts: np.ndarray, counts_matrix: np.ndarray, thicknesses: np.ndarray, bounds_t: np.ndarray,
                        bounds_a: np.ndarray, low_e_limit_index: int, use_linear_initial_fit=True) -> MIMBSFitResult:
    """Estimate MIMBS isotope and absorber densities for a test histogram, 
    solving the nonlinear least squares equation under the variance stabilising sqrt transform

    :param counts: test histogram
    :param counts_matrix: 4d matrix of source, absorber, thickness, histogram
    :param thicknesses: 3d matrix of source, absorber, thickness
    :param bounds_t: 2 tuple/array of min and max bounds for thickness values
    :param bounds_a: 2 tuple/array of min and max bounds for isotope compositions
    :param low_e_limit_index: energy below which to avoid fitting due to k-edge effects
    """
    assert (counts >= 0).all(), "Counts matrix must be positive real"
    counts_transform = np.sqrt(counts)
    args_nonlinear = counts_transform, counts_matrix, thicknesses, bounds_a, low_e_limit_index

    if use_linear_initial_fit:
        args_linear = counts, counts_matrix, thicknesses, low_e_limit_index
        t0_fit = differential_evolution(residual_for_t_linear, bounds=bounds_t.T, args=args_linear)
        print(t0_fit)
        

    else:
        # Find global minimum initial parameters
        t0_fit = differential_evolution(ssr_given_t, bounds=bounds_t.T, args=args_nonlinear)

    # Perform local minimization
    t_fit = least_squares(fit_residuals_given_t, t0_fit.x, bounds=bounds_t, args=args_nonlinear)
    a_fit = fit_for_t(t_fit.x, *args_nonlinear)
    
    # Reconstruct fit histogram from product of response matrix and composition basis
    R = R_t(t_fit.x, counts_matrix, thicknesses)
    
    return MIMBSFitResult(R@a_fit.x, a_fit.x, t_fit.x, 2*a_fit.cost)


