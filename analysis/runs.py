from ujson import loads
from pathlib import Path
from typing import Dict, NamedTuple, List, Any, Tuple, Sequence

import numpy as np

SUPPORT_LEGACY = True


class AggregateRunInfo(NamedTuple):
    version: Tuple[int, int, int]
    specification_path: str
    specification_kwargs: Dict[str, Any]
    macro_paths: List[str]
    dependency_paths: List[str]


class RunInfo(NamedTuple):
    version: Tuple[int, int, int]
    specification_path: str
    specification_kwargs: Dict[str, Any]
    macro_paths: List[str]
    dependency_paths: List[str]
    metric_type: str
    metric_limit: int
    job_metric_limit: int


class Run(NamedTuple):
    collections: Dict[str, Sequence]
    events: int
    detections: int
    n_jobs: int
    run_info: RunInfo


class AggregateRun(NamedTuple):
    collections: Dict[str, Sequence]
    events: int
    detections: int
    n_jobs: int
    run_info: AggregateRunInfo
        

def aggregate_runs(*runs, ignore_compatability=False):
    if not runs:
        raise ValueError(f"No runs found for {directories}")

    first_run = runs[0]
    run_info = AggregateRunInfo(*first_run.run_info[:5])

    # Basic run info must equate
    if not ignore_compatability:
        assert all(r.run_info[:5] == run_info for r in runs[1:])

    first_collections = first_run.collections
    collections = {n: np.concatenate([r.collections[n] for r in runs]) for n in first_collections}

    events = sum(m.events for m in runs)
    detections = sum(m.detections for m in runs)
    n_jobs = sum(r.n_jobs for r in runs)

    return AggregateRun(collections, events, detections, n_jobs, run_info)


def load_run(directory: Path) -> Run:
    run_info_path = directory / "run_info.json"
    if not run_info_path.exists():
        raise FileNotFoundError(f"Could not find {run_info_path}")

    run_info_data = loads(run_info_path.read_text())
    run_info = RunInfo(**run_info_data)

    collections = None
    n_jobs = 0
    events = 0
    detections = 0

    # Load each job
    for path in directory.glob("*.json"):
        if path == run_info_path:
            continue

        data = loads(path.read_text())

        job_result = data['result']
        if not job_result:
            raise ValueError(f"Job missing result: {path}")

        # Aggregate job collections
        job_collections = job_result['collections']
        if collections is None:
            collections = job_collections

        else:
            for k, v in job_collections.items():
                collections[k] += v

        n_jobs += 1
        events += job_result['events']
        detections += job_result['detections']

    if not n_jobs:
        raise ValueError(f"Directory was empty:  {directory}")

    collections = {n: np.array(v) for n, v in collections.items()}
    return Run(collections, events, detections, n_jobs, run_info)


async def _wait_then_set_event(delay: float, event):
    import asyncio
    await asyncio.sleep(delay)
    event.set()


async def _run_watcher(directory: Path, aggregation_time: float, event):
    import aionotify
    import asyncio

    # Setup the watcher
    watcher = aionotify.Watcher()
    watcher.watch(alias='live_update', path=str(directory), flags=aionotify.Flags.CREATE)

    # Prepare the loop
    loop = asyncio.get_event_loop()
    result_loader: asyncio.Task = None

    await watcher.setup(loop)

    try:
        while True:
            await watcher.get_event()
            if result_loader is not None:
                result_loader.cancel()

            result_loader = asyncio.ensure_future(_wait_then_set_event(aggregation_time, event))

    finally:
        watcher.close()


async def realtime_iter_runs(directory: Path, aggregation_time: float):
    import asyncio

    has_result = asyncio.Event()
    watcher_task = asyncio.ensure_future(_run_watcher(directory, aggregation_time, has_result))

    try:
        try:
            yield load_run(directory)
        except ValueError:
            pass

        while True:
            await has_result.wait()
            has_result.clear()
            yield load_run(directory)

    finally:
        watcher_task.cancel()
