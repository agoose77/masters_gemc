import pathlib
import types
from typing import Tuple
import numpy as np

from swarm.common.spe import read_spe


def load_spe(path: pathlib.Path) -> types.SimpleNamespace:
    with open(path) as f:
        blocks = list(read_spe(f))

    blocks_namespace = {type(b).__name__.lower(): b for b in blocks}
    return types.SimpleNamespace(**blocks_namespace)


def hist_from_spe(spe: types.SimpleNamespace) -> Tuple[np.ndarray, np.ndarray]:
    x = np.arange(spe.data.min, spe.data.max+1)
    y = spe.data.data
    
    return x, y