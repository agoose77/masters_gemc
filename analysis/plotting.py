from itertools import cycle, islice
from typing import Tuple, List, Union, Callable

from contextlib import contextmanager
from collections import Iterable

from bokeh.plotting import Figure, figure, show
from bokeh.models import GlyphRenderer
from bokeh.io import export_svgs
from pathlib import Path

import subprocess


def create_palette_cycler(palette, size: int):
    index = min(max(palette), max(min(palette), size))
    return cycle(palette[index])


def get_n_colours(palette, size: int) -> Tuple[str]:
    cycler = create_palette_cycler(palette, size)
    return tuple(islice(cycler, size))


class Context:

    def __init__(self, figure):
        self.figure = figure
        self._grouped_renderers = []
        self._tracked_renderers = set()

    def _sync_untracked_renderers(self):
        untracked = [b for b in self._glyph_renderers if not b in self._tracked_renderers]
        self._grouped_renderers.extend(untracked)
        self._tracked_renderers.update(untracked)

    @property
    def _glyph_renderers(self):
        return [r for r in self.figure.renderers if hasattr(r, 'glyph')]

    @contextmanager
    def group(self):
        before = self._glyph_renderers
        self._sync_untracked_renderers()
        yield
        new_renderers = set(self._glyph_renderers).difference(before)
        self._grouped_renderers.append(new_renderers)
        self._tracked_renderers.update(new_renderers)

    @property
    def grouped_renderers(self):
        self._sync_untracked_renderers()
        return self._grouped_renderers

    
def default_svg_to_png(svg_path, png_path):
    subprocess.check_call(['svg2png', str(svg_path), '-o', str(png_path)])

def inkscape_svg_to_png(svg_path, png_path):
    subprocess.check_call(['inkscape', str(svg_path), '-z', '-e', str(png_path)])
    

class Plotting:

    def __init__(self, params: dict, palette: list, output_image: bool = False, svg_to_png: Callable[[Path, Path], None] = inkscape_svg_to_png, destroy_svg: bool=True):
        self.params = params
        self.palette = palette
        self.output_image = output_image
        self.svg_to_png = svg_to_png
        self.destroy_svg = destroy_svg
        self._context = None

    @contextmanager
    def group(self):
        with self._context.group():
            yield

    @contextmanager
    def figure(self, *args, output_path: Path = None, auto_colour: bool = True, **kwargs) -> Figure:
        params = {**self.params, **kwargs}
        if output_path is not None:
            params['output_backend'] = 'svg'
         
        fig = figure(*args, **params)
        self._context, ctx = Context(fig), self._context
        yield fig
        self._context, ctx = ctx, self._context
        
        grouped_renderers = ctx.grouped_renderers
        if auto_colour:
            self._set_auto_colours(grouped_renderers)
        
        self._display(fig, output_path)

    @contextmanager
    def chart(self, cls, *args, output_path: Path = None, **kwargs) -> Figure:
        fig = cls(*args, **kwargs, **self.params, **self.default_params)
        yield fig
        self.display(fig, output_path)

    def _display(self, fig: Figure, path: Path = None):
        show(fig)

        if path is not None and self.output_image:
            if path.suffix.lower() == '.png':
                self._output_png(fig, path)
            elif path.suffix.lower() == '.svg':
                export_svgs(fig, path)
            else:
                raise ValueError(f"Invalid extension {path.suffix}")

    def _output_png(self, fig: Figure, path: Path):
        title = fig.title
        fig.title = None

        try:
            svg_path = path.parent / (path.stem + ".svg")
            export_svgs(fig, svg_path)
            try:
                # Unlink existing
                if path.exists():
                    path.unlink()
                self.svg_to_png(svg_path, path)
            finally:
                if self.destroy_svg:
                    svg_path.unlink()
        finally:
            fig.title = title

    def _set_auto_colours(self, grouped_renderers: List[Union[GlyphRenderer, List[GlyphRenderer]]]):
        for renderer_or_renderers, colour in zip(grouped_renderers,
                                                 get_n_colours(self.palette, len(grouped_renderers))):
            if isinstance(renderer_or_renderers, Iterable):
                renderers = renderer_or_renderers
            else:
                renderers = [renderer_or_renderers]
                
            for renderer in renderers:
                renderer.glyph.line_color = colour
                if hasattr(renderer.glyph, 'fill_color'):
                    renderer.glyph.fill_color = colour