from collections import defaultdict
from csv import reader
from re import compile
from typing import NamedTuple, Union, Dict, List, Type, Sequence, Tuple
from operator import attrgetter
from itertools import chain
from pathlib import Path


class PhotopeakInfo(NamedTuple):
    energy: float
    probability: float
    origin: str


_nuclide_name_pattern = compile(r"^\d+-(\w+)-(\d+\w?)$")
_key_probability = attrgetter('probability')


def _parse_nuclide_name(string: str) -> Union[str, None]:
    match = _nuclide_name_pattern.match(string)
    if match is None:
        return

    element, neutrons = match.groups((2, 3))
    return f"{element}{neutrons}"


def _coerce(t: Type, x):
    try:
        return t(x)
    except ValueError:
        return None


def _parse_table(path: Path, field_name_types: Sequence[Tuple[str, Type]], origin: str) -> Dict[str, List[PhotopeakInfo]]:
    nuclide = None
    table = defaultdict(list)

    with open(path) as f:
        for row in reader(f):
            if not any(row):
                continue

            first, *fields = row
            if first:
                nuclide = _parse_nuclide_name(first)

            if nuclide is None:
                continue

            as_dict = {n: _coerce(t, x) for (n, t), x in zip(field_name_types, fields)}
            photopeak = PhotopeakInfo(as_dict['energy'], as_dict['probability'], origin)
            table[nuclide].append((photopeak))

    for photopeaks in table.values():
        photopeaks.sort(key=_key_probability, reverse=True)

    table.default_factory = None
    return table


def _float_or_range(string):
    try:
        return float(string)
    except ValueError:
        x, y = string.split('-')
        return float(x), float(y)


_THIS_FILE_DIR = Path(__file__).parent
GAMMA_TABLE_PATH = _THIS_FILE_DIR / "gamma_table.csv"
XRAY_TABLE_PATH = _THIS_FILE_DIR / "xray_table.csv"


def load_gamma_table() -> Dict[str, List[PhotopeakInfo]]:
    field_name_types = [('energy', _float_or_range),
                        ('uncertainty_energy', float),
                        ('probability', float),
                        ('uncertainty_probability', float),
                        ('comments', str)]

    return _parse_table(GAMMA_TABLE_PATH, field_name_types, 'gamma')


def load_xray_table() -> Dict[str, List[PhotopeakInfo]]:
    field_name_types = [('source', str),
                        ('shell', str),
                        ('energy', _float_or_range),
                        ('uncertainty_energy', float),
                        ('probability', float),
                        ('uncertainty_probability', float),
                        ('comments', str)]

    return _parse_table(XRAY_TABLE_PATH, field_name_types, 'xray')


def load_aggregate_table() -> Dict[str, List[PhotopeakInfo]]:
    table = defaultdict(list)
    for isotope, values in chain(load_gamma_table().items(), load_xray_table().items()):
        table[isotope].extend(values)

    for photopeaks in table.values():
        photopeaks.sort(key=_key_probability, reverse=True)

    table.default_factory = None
    return table