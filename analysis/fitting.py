from functools import partial
from inspect import signature
from itertools import islice
from typing import NamedTuple, Sequence, Tuple, Union, Callable, Any, Dict

import numpy as np
from scipy.optimize import curve_fit
from scipy.odr import Model, RealData, ODR, Output


class FitResult(NamedTuple):
    params: np.ndarray
    sigmas: np.ndarray


class CalibrationResult(NamedTuple):
    """Calibration result object.

    :field odr_result: ODR Output object
    :field source_to_fits: mapping from source identifier to fit_many_gauss FitResult object
    """
    odr_result: Output
    source_to_fits: Dict[str, FitResult]


def fit_odr(fit_func, x: np.ndarray, y: np.ndarray, sx: np.ndarray=None, sy: np.ndarray=None) -> Output:
    def _fit_func(B, x):
        return fit_func(x, *B)

    model = Model(_fit_func)
    data = RealData(x, y, sx=sx, sy=sy)

    n_params = len(signature(fit_func).parameters) - 1
    odr = ODR(data, model, beta0=np.ones(n_params))
    return odr.run()


def calibrate_from_estimates(fit_func, source_to_fit_params: Dict[Any, Dict[str, np.ndarray]],
                             source_to_hist: Dict[Any, Tuple[np.ndarray, np.ndarray]]) -> CalibrationResult:
    """Perform calibration, first fitting gaussians to initial centroid estimates,
    then finding the ODR fit for the centroids vs energies

    :param fit_func: function to fit for calibration
    :param source_to_fit_params: dictionary of identifier to fit parameters dictionary
    of {'centroids': [...], 'energies': [...]}
    :param source_to_hist: mapping of identifier to x, y histogram arrays
    """
    source_to_fit_energies_pairs = {}

    for source, params in source_to_fit_params.items():
        x, y = source_to_hist[source]
        centroids = params['centroids']
        energies = params['energies']

        fit = fit_many_gauss(x, y, centroids)
        source_to_fit_energies_pairs[source] = fit, energies

    fits, energies = zip(*source_to_fit_energies_pairs.values())
    all_centroids = np.concatenate([f.params[1::3] for f in fits])
    all_centroid_errors = np.concatenate([f.sigmas[1::3] for f in fits])
    all_energies = np.concatenate(energies)

    odr_fit = fit_odr(fit_func, all_centroids, all_energies, sx=all_centroid_errors)
    source_to_fits = {s: f for s, (f, e) in source_to_fit_energies_pairs.items()}
    return CalibrationResult(odr_fit, source_to_fits)


def fit_many_gauss(x: np.ndarray, y: np.ndarray, centroids: np.ndarray, max_deviation: float=3.0, sigma_factor=1.0) -> FitResult:
    indices = np.searchsorted(x, centroids)
    p_0 = np.array([y[indices], centroids, np.sqrt(centroids)*sigma_factor]).T.reshape(len(centroids) * 3)
    p_opt, p_cov = curve_fit(many_gauss, x, y, p_0)
    p_err = np.absolute(np.diag(p_cov))**0.5

    refined_centroids = p_opt[1::3]
    err_centroids = p_err[1::3]
    std_dev = np.abs(p_opt[2::3])

    invalid_mask = np.abs(refined_centroids-centroids) > std_dev*max_deviation
    if invalid_mask.any():
        print(np.abs(refined_centroids-centroids)[invalid_mask], (std_dev*max_deviation)[invalid_mask])
        raise ValueError(f"Initial centroid estimate too far from final value: {centroids[invalid_mask], refined_centroids[invalid_mask]}")

    return FitResult(p_opt, p_err)


def _root_histogram_from_bin_counts(bin_counts: np.ndarray):
    import ROOT
    hist = ROOT.TH1F("hist", "<file>", len(bin_counts), 0, len(bin_counts) - 1)
    for i, x in enumerate(bin_counts):
        hist.SetBinContent(i, x)
    ROOT.SetOwnership(hist, True)
    return hist


class PeakSearchResult(NamedTuple):
    n_peaks: int
    centroids: np.ndarray


def find_peaks(bin_counts: np.ndarray, max_peaks: int, threshold: float, sigma: float=3) -> PeakSearchResult:
    """Find N peaks in spectrum using Root TSpectrum.Search"""
    import ROOT
    spec = ROOT.TSpectrum(max_peaks)
    ROOT.SetOwnership(spec, True)
    hist = _root_histogram_from_bin_counts(bin_counts)
    n_found = spec.Search(hist, sigma, "", threshold)
    centroids = np.fromiter(spec.GetPositionX(), np.float_, count=n_found)
    return PeakSearchResult(n_found, centroids)


def gauss(x, a: float, mu: float, stddev: float):
    """Gaussian function."""
    return a * np.exp(-(x - mu) ** 2 / (2 * stddev ** 2))


def many_gauss(x: np.ndarray, *args) -> np.ndarray:
    """A sum of Gaussian functions"""
    assert not len(args) % 3

    y = np.zeros_like(x)
    for i in range(0, len(args), 3):
        b = args[i:i + 3]
        y += gauss(x, *b)
    return y


def polynomial(x, *params):
    tot = np.zeros_like(x, dtype=np.float_)
    for i, c in enumerate(reversed(params)):
        tot += c * x**i
    return tot


def linear(x, m, c):
    return polynomial(x, m, c)


def quadratic(x, a, b, c):
    return polynomial(x, a, b, c)


def cubic(x, a, b, c, d):
    return polynomial(x, a, b, c, d)


def quartic(x, a, b, c, d, e):
    return polynomial(x, a, b, c, d, e)


def quintic(x, a, b, c, d, e, f):
    return polynomial(x, a, b, c, d, e, f)


class Resolution(NamedTuple):
    r: Union[float, np.ndarray]
    dr: Union[float, np.ndarray]


def resolution_from_gaussian_fit(fit_result: FitResult) -> Resolution:
    """Determine resolution(s) from Gaussian fit result(s)

    Where multiple Gaussian fits are contained in the fit parameters (such as fitting many_gauss), an array of resolutions and errors are returned
    """
    assert not len(fit_result.params) % 3
    assert not len(fit_result.sigmas) % 3
    _, μ, σ = fit_result.params.reshape((-1, 3)).T
    _, dμ, dσ = fit_result.sigmas.reshape((-1, 3)).T
    
    f = 2 * np.sqrt(2 * np.log(2))
    R = f * σ / μ
    dR = R * ((dσ / σ) ** 2 + (dμ / μ) ** 2) ** 0.5
    return Resolution(R, dR)


def apply(f, params) -> Callable:
    """Bind fit parameters to fit function"""
    _, *param_names = signature(f).parameters
    assert len(param_names) == len(params)
    return partial(f, **{n: v for n, v in zip(param_names, params)})
