from pathlib import Path
from re import compile as re_compile
from typing import Iterable, Iterator
from collections import defaultdict


def groupby(collection: list, name: str) -> dict:
    """Group elements in list by attribute values"""
    result = defaultdict(list)
    for obj in collection:
        key = getattr(obj, name)
        result[key].append(obj)

    result.default_factory = None
    return result


def match_regex(candidates: Iterable[str], pattern: str) -> Iterator[tuple]:
    matcher = re_compile(pattern)
    for candidate in candidates:
        match = matcher.match(candidate)
        if match is not None:
            yield match, candidate


def glob_regex(path: Path, pattern: str) -> Iterator[tuple]:
    matcher = re_compile(pattern)
    for candidate in path.glob("*"):
        match = matcher.match(candidate.name)
        if match is not None:
            yield match, candidate
